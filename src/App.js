//import UserCard from "./components/UserCard";
//import Gallery, { Profile } from "./components/Profile";
//import { Container, Row } from "react-bootstrap";
//import ProfileCardWrapper from "./components/ProfileCardWrapper";
//import Avatar from "./components/Avatar";
// import AdminGreeting from "./components/AdminGreeting";
// import UserGreeting from "./components/UserGreeting";

// import { Container, Row } from "react-bootstrap";
// import HelloGreeting from "./components/HelloGreeting";
// import NotFound from "./components/NotFound";
// import Task from "./components/Task";
//import CounterFuncComponent from "./components/CounterFuncComponent";
//import HandleEvent from "./components/HandleEvent";
//import HandleEvent from "./components/HandleEvent";
import { BrowserRouter } from "react-router-dom";
//import ControlledComponent from "./ccomponents/ControlledComponent";
import NavbarShow from "./components/Navbar";
//import HandleEventClassComponent from "./components/HandleEventClassComponent.";
//import LoginButton from "./components/LoginButton";
//import HandleEventClassComponent from "./components/HandleEventClassComponent.";
import HomePage from "./pages/HomePage";
import { Route, Routes } from "react-router-dom";
import ProductPage from "./pages/ProductPage";
import ServicePage from "./pages/ServicePage";
import AboutPage from "./pages/AboutPage";
import ContactPage from "./pages/ContactPage";
import NotfoundPage from "./pages/NotfoundPage";
import ProductDetail from "./components/ProductDetail";

//const messageOut = "Hello React from outside!";

// function App() {
//   const message = "Hello React Developer!";

//   let users = [
//     {
//       name: "dara",
//       email: "dara@gmail.com",
//       phone: "09876542",
//       imageUrl: "https://www.jenkins.io/images/logos/amsterdam/amsterdam.png",
//     },
//     {
//       name: "pheak",
//       email: "pheak@gmail.com",
//       phone: "09873442",
//       imageUrl:
//         "https://static.vecteezy.com/system/resources/thumbnails/025/934/636/small_2x/cute-pokemon-background-icon-generated-by-ai-photo.jpg",
//     },
//     {
//       name: "jack",
//       email: "jack@gmail.com",
//       phone: "09876782",
//       imageUrl:
//         "https://static.vecteezy.com/system/resources/thumbnails/025/934/636/small_2x/cute-pokemon-background-icon-generated-by-ai-photo.jpg",
//     },
//     {
//       name: "jeat",
//       email: "jeat@gmail.com",
//       phone: "09876799",
//       imageUrl: "https://www.jenkins.io/images/logos/alien/alien.png",
//     },
//     {
//       name: "sok",
//       email: "sok@gmail.com",
//       phone: "09876567",
//       imageUrl:
//         "https://static.vecteezy.com/system/resources/thumbnails/025/934/636/small_2x/cute-pokemon-background-icon-generated-by-ai-photo.jpg",
//     },
//     {
//       name: "june",
//       email: "june@gmail.com",
//       phone: "098767676",
//       imageUrl: "https://www.jenkins.io/images/logos/alien/alien.png",
//     },
//   ];

//   const people = [
//     "Creola Katherine Johnson: mathematician",
//     "Mario José Molina-Pasquel Henríquez: chemist",
//     "Mohammad Abdus Salam: physicist",
//     "Percy Lavon Julian: chemist",
//     "Subrahmanyan Chandrasekhar: astrophysicist",
//   ];

//   const listItems = people.map((person, index) => (
//     <li key={index}>{person}</li>
//   ));

//   return (
//     <>
//       <h1>Hello World</h1>
//       <h1>Hello from React</h1>
//       <p>
//         Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere
//         explicabo neque, iste modi voluptatem fugit mollitia nobis pariatur
//         velit reiciendis asperiores voluptates, aliquam tenetur architecto
//         deserunt veritatis atque sed illum?
//       </p>
//       <h1>
//         {message} and {messageOut}
//       </h1>

//       <Avatar />
//       <Profile />
//       <Gallery />

//       <Container fluid>
//         <Row className="g-2 mt-2">
//           {users.map((user, index) => {
//             return <ProfileCardWrapper user={user} key={index} />;
//           })}
//         </Row>
//       </Container>

//       {/* This is where to call UserCard component */}
//       <div className="container">
//         <UserCard name="user1" random="12345" />
//         <UserCard name="user2" random="34342" />
//         <UserCard name="user3" random="34637" />
//         <UserCard name="user4" random="34638" />
//         <UserCard name="user5" random="34996" />
//       </div>

//       <div>
//         <ul>{listItems}</ul>
//       </div>
//     </>
//   );
// }

// function App() {
//   const isAdmin = true;

//   let tasks = [
//     {
//       taskName: "Task 1",
//       taskDescription: "This is the task 1",
//     },
//     {
//       taskName: "Task 2",
//       taskDescription: "This is the task 2",
//     },
//     {
//       taskName: "Task 3",
//       taskDescription: "This is the task 3",
//     },
//     {
//       taskName: "Task 4",
//       taskDescription: "This is the task 4",
//     },
//     {
//       taskName: "Task 5",
//       taskDescription: "This is the task 5",
//     },
//   ];

//   //tasks = new Array();

//   return (
//     <div>
//       <h1>Header section</h1>
//       {/* {isAdmin ? <AdminGreeting /> : <UserGreeting />} */}
//       {/* <HelloGreeting isAdmin={false} /> */}
//       <HelloGreeting isAdmin={isAdmin} />

//       {/* conditional rendering */}
//       {tasks.length !== 0 ? ( //you can use &&
//         <Container fluid>
//           <Row className="g-2 mt-2">
//             {tasks.map((task, index) => (
//               <Task key={index} task={task} />
//             ))}
//           </Row>
//         </Container>
//       ) : (
//         <NotFound />
//       )}
//     </div>
//   );
// }

const App = () => {
  return (
    <>
      <BrowserRouter>
        <NavbarShow />
        <Routes>
          <Route index path="/" element={<HomePage />} />
          <Route extact path="/product" element={<ProductPage />}>
            <Route path=":productId" element={<ProductDetail />} />
          </Route>
          <Route path="/service" element={<ServicePage />} />
          <Route path="/about" element={<AboutPage />} />
          <Route path="/contact" element={<ContactPage />} />
          <Route path="/*" element={<NotfoundPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;

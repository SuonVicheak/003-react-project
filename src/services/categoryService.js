import { api } from "../utils/api";

export const getAllCategories = async () => {
  let response = await api.get("categories");
  return response.data;
};

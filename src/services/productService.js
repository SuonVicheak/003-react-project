import { api } from "./../utils/api";

export const getAllProducts = async () => {
  let response = await api.get("products");
  return response.data;
};

export const getProductById = async (id) => {
  let response = await api.get("products/" + id);
  return response.data;
};

export const createNewProduct = async (productData) => {
  let response = await api.post("products", productData);
  return response.data;
};

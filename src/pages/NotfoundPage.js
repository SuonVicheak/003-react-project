import React from "react";
import { NavLink } from "react-router-dom";

const NotfoundPage = () => {
  return (
    <div className="container text-center">
      <div>
        <img
          src="https://static.vecteezy.com/system/resources/thumbnails/024/217/744/small_2x/design-template-for-web-page-with-404-error-isometric-page-not-working-error-png.png"
          alt=""
          className="img-fluid"
        />
      </div>

      <NavLink to={"/"} className={"btn btn-primary mt-3"}>
        Back to Home
      </NavLink>
    </div>
  );
};

export default NotfoundPage;

import React, { useEffect, useState } from "react";
import { getAllProducts } from "./../services/productService";
import ProductCard from "../components/ProductCard";
import Loading from "../components/Loading";
import { Outlet } from "react-router-dom";

const ProductPage = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    getAllProducts()
      .then((data) => {
        setProducts(data);
        return data;
      })
      .catch((error) => console.log(error));
  }, []);

  //console.log(products);

  let productLoadings = [];
  for (let i = 0; i < 20; i++) {
    productLoadings.push(<Loading key={i} />);
  }

  return (
    <div className="container">
      <h3>List of All Products</h3>
      <Outlet />
      <div className="container mt-4">
        <div className="row">
          {products.length === 0 && productLoadings}
          {products.map((product, index) => {
            return <ProductCard product={product} key={index} />;
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductPage;

import React, { useEffect, useState } from "react";
//import { useParams } from "react-router-dom";
//import axios from "axios";

const ContactPage = () => {
  //const { params } = useParams();
  //console.log(`Params : ${params}`);
  //const [products, setProducts] = useState([]);

  //using useEffect to prevent from infinite rerendering
  //first param => callback function
  //second param => dependencies
  //useEffect(() => {
  //using fetch API to communicate with backend servers
  /*fetch("https://api.escuelajs.co/api/v1/products/", {
      method: "GET",
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json(); //parse the JSON response
      })
      .then((data) => {
        //store the parsed data in an array of objects
        setProducts(data);
        return data;
      })
      .catch((error) => console.log(error));*/

  //   axios
  //     .get("https://api.escuelajs.co/api/v1/products/")
  //     .then((response) => {
  //       setProducts(response.data);
  //       return response;
  //     })
  //     .catch((error) => console.log(error));
  // }, []);

  //console.log(products);

  const [counter, setCounter] = useState(0);
  const [student, setStudent] = useState({
    name: "dara",
    gender: "male",
    grade: 12,
  });
  const [item, setItem] = useState(null);
  const [items, setItems] = useState(["item1", "item2", "item3"]);

  useEffect(() => {
    console.log(`Counter : ${counter}`);
  }, [counter]);

  const handleCounter = () => {
    //setCounter(counter + 1);
    /*setCounter((preCounter) => {
      return preCounter + 1;
    });*/
    setCounter((preCounter) => preCounter + 1);
    //counter += 1;
  };

  const handleChange = ({ target }) => {
    //change state as an object
    //console.log(target);
    //console.log(e);
    const { name, value } = target;
    //safe state modification
    setStudent((preStudent) => {
      return {
        ...preStudent,
        [name]: value,
      };
    });
  };

  const handleSubmit = () => {};

  const handleUpdateItem = ({ target }) => {
    setItem(target.value);
  };

  const handleAddNewItem = () => {
    if (item) {
      setItems((preItem) => [...preItem, item]);
      setItem(null);
    } else {
      alert("Please enter item name!");
    }
  };

  return (
    <div className="container">
      <h3 className="my-5">This is Contact Page</h3>
      <h3 className="mx-5">{counter}</h3>
      <div>
        <button className="btn btn-danger mb-4" onClick={handleCounter}>
          Click Counter
        </button>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="name">Name : {student.name}</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              value={student.name}
              onChange={handleChange}
            />
          </div>
          <div>
            <label htmlFor="gender">Gender : {student.gender}</label>
            <input
              type="text"
              className="form-control"
              id="gender"
              name="gender"
              value={student.gender}
              onChange={handleChange}
            />
          </div>
          <div>
            <label htmlFor="grade">Grade : {student.grade}</label>
            <input
              type="text"
              className="form-control"
              id="grade"
              name="grade"
              value={student.grade}
              onChange={handleChange}
            />
          </div>
        </form>
      </div>
      <div>
        {items.length === 0
          ? "No items found..."
          : items.map((item, index) => <li key={index}>{item}</li>)}
        <input
          type="text"
          className="form-control"
          id="item"
          name="item"
          value={item ? item : ""}
          onChange={handleUpdateItem}
        />
        <button className="btn btn-danger my-3" onClick={handleAddNewItem}>
          Add New Item
        </button>
      </div>
    </div>
  );
};

export default ContactPage;

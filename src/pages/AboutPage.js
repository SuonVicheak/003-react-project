import React from "react";
import AboutComponent from "../components/AboutComponent";

const AboutPage = () => {
  return (
    <div className="container">
      <h3 className="my-5">This is About Page</h3>
      <AboutComponent />
    </div>
  );
};

export default AboutPage;

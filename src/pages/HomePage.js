import React, { useEffect, useState } from "react";
import { getAllCategories } from "./../services/categoryService";
import { createNewProduct } from "../services/productService";
import AlertShowBox from "../components/Alert";

const HomePage = () => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [categoryId, setCategoryId] = useState(null);
  const [categories, setCategories] = useState([]);
  const [isAlert, setIsAlert] = useState(false);

  useEffect(() => {
    getAllCategories()
      .then((data) => {
        setCategories(data);
        return data;
      })
      .catch((error) => console.log(error));
  }, []);

  //console.log(categories);

  const handleSubmit = (e) => {
    e.preventDefault();

    const newProduct = {
      title: title,
      price: 100,
      description: description,
      categoryId: Number(categoryId),
      images: ["https://placeimg.com/640/480/any"],
    };

    //console.log(newProduct);

    createNewProduct(newProduct)
      .then((data) => {
        console.log(data);
        setTitle("");
        setDescription("");
        setIsAlert(true);
      })
      .catch((error) => console.log(error));
  };

  return (
    <div className="container">
      <h3>Home Page - Add New Product</h3>
      <div>
        <form onSubmit={handleSubmit} className="mb-4">
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Product Title
            </label>
            <input
              type="title"
              className="form-control"
              id="title"
              placeholder="My Product"
              required={true}
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="description" className="form-label">
              Product Description
            </label>
            <textarea
              className="form-control"
              id="description"
              rows="4"
              required={true}
              value={description}
              onChange={(e) => setDescription(e.target.value)}
            ></textarea>
          </div>
          <div className="mb-3">
            <label htmlFor="description" className="form-label">
              Product Category
            </label>
            <select
              className="form-select"
              aria-label="Default select example"
              required={true}
              onChange={(e) => setCategoryId(e.target.value)}
            >
              {categories.map((category, index) => {
                return (
                  <option key={index} value={category.id}>
                    {category.name}
                  </option>
                );
              })}
            </select>
          </div>
          <div className="d-flex justify-content-end align-content-between">
            <button className="btn btn-primary">Create New Product</button>
          </div>
        </form>
        {isAlert && <AlertShowBox message="Product created successfully!" />}
      </div>
    </div>
  );
};

export default HomePage;

import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";

function ProductCard({ product }) {
  const { id, title, description, images, category } = product;

  return (
    <Card className="col-xl-3 col-lg-3 col-md-4 col-sm-6 my-3">
      <Card.Img
        variant="top"
        src={
          images.length === 0
            ? "https://media.istockphoto.com/id/1409329028/vector/no-picture-available-placeholder-thumbnail-icon-illustration-design.jpg?s=612x612&w=0&k=20&c=_zOuJu755g2eEUioiOUdz_mHKJQJn-tDgIAhQzyeKUQ="
            : images[0]
        }
        className="p-3 pt-4"
      />
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>{description}</Card.Text>
        <Link className="btn btn-primary" to={"/product/" + id}>
          {category.name}
        </Link>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;

import React, { Component } from "react";

export class ModeComponent extends Component {
  render() {
    const lightMode = {
      width: "300px",
      height: "300px",
      backgroundColor: "gray",
      color: "white",
      fontSize: "1.2rem",
      textTransform: "uppercase",
      borderRadius: "20%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    };

    const darkMode = {
      width: "300px",
      height: "300px",
      backgroundColor: "black",
      color: "white",
      fontSize: "1.2rem",
      textTransform: "uppercase",
      borderRadius: "20%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    };

    return (
      <div>
        {this.props.darkMode ? (
          <div style={darkMode}>Dark Mode</div>
        ) : (
          <div style={lightMode}>Light Mode</div>
        )}
      </div>
    );
  }
}

export default ModeComponent;

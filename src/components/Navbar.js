import { Button } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { NavLink } from "react-router-dom";

function NavbarShow() {
  return (
    <Navbar expand="lg" className="bg-body-tertiary mb-3 fixed-top">
      <Container>
        <Navbar.Brand href="#home">My Service</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <NavLink to={"/"} className={"nav-link"}>
              Home
            </NavLink>
            <NavLink to={"/product"} className={"nav-link"}>
              Products
            </NavLink>
            <NavLink to={"/service"} className={"nav-link"}>
              Services
            </NavLink>
            <NavLink to={"/about"} className={"nav-link"}>
              About Me
            </NavLink>
            <NavLink to={"/contact"} className={"nav-link"}>
              Contact Us
            </NavLink>
          </Nav>
          <Button className="mx-2" variant="warning">
            Sign Up
          </Button>
          <Button variant="success">Login</Button>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavbarShow;

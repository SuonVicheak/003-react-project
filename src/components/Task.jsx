import React from "react";
import { Button, Card, Col } from "react-bootstrap";

const Task = ({ task }) => {
  const { taskName, taskDescription } = task;
  return (
    <Col
      xs={12}
      sm={12}
      md={6}
      lg={4}
      xl={3}
      className="d-flex align-item-center justify-content-center"
    >
      <Card style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="https://cdn-images.threadless.com/threadless-media/artist_shops/shops/designhq/products/743926/shirt-1536157530-e326cd9045b7ad853a7a731a1b00baf1.png?v=3&d=eyJvcHMiOiBbWyJ0cmltIiwgW2ZhbHNlLCBmYWxzZV0sIHt9XSwgWyJyZXNpemUiLCBbXSwgeyJ3aWR0aCI6IDk5Ni4wLCAiYWxsb3dfdXAiOiBmYWxzZSwgImhlaWdodCI6IDk5Ni4wfV0sIFsiY2FudmFzX2NlbnRlcmVkIiwgWzEyMDAsIDEyMDBdLCB7ImJhY2tncm91bmQiOiAiZmZmZmZmIn1dLCBbInJlc2l6ZSIsIFs4MDBdLCB7fV0sIFsiY2FudmFzX2NlbnRlcmVkIiwgWzgwMCwgODAwLCAiI2ZmZmZmZiJdLCB7fV0sIFsiZW5jb2RlIiwgWyJqcGciLCA4NV0sIHt9XV0sICJmb3JjZSI6IGZhbHNlLCAib25seV9tZXRhIjogZmFsc2V9"
        />
        <Card.Body>
          <Card.Title>{taskName}</Card.Title>
          <Card.Text>{taskDescription}</Card.Text>
          <Button variant="primary">Explore More</Button>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default Task;

import { useState } from "react";
import Alert from "react-bootstrap/Alert";

const AlertShowBox = (props) => {
  const [show, setShow] = useState(true);
  const { message } = props;

  return (
    <Alert variant="danger" onClose={() => setShow(!show)} dismissible>
      <p>{message}</p>
    </Alert>
  );
};

export default AlertShowBox;

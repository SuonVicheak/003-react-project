import React from "react";
import "../styles/UserCard.css";

//styling in react js
//inline style
//object style
//external style

//object destructuring
const UserCard = ({ name, random }) => {
  const userCardHeaderStyle = {
    margin: 0,
  };

  return (
    <div
      className="container user-card-container w-100"
      style={{
        backgroundColor: "yellowgreen",
        color: "white",
        padding: "20px",
        margin: "20px",
        borderRadius: "20px",
      }}
    >
      <h1 style={userCardHeaderStyle}>User Card {name}</h1>
      <h3>This is the description of user card with random {random}</h3>
    </div>
  );
};

export default UserCard;

//type of exporting in javascript
//1. default export
//2. named export
//3. mixed export (both default and named export)

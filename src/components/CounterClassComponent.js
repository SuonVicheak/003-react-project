import React, { Component } from "react";

//class component => statefull component
export class CounterClassComponent extends Component {
  constructor(props) {
    super(props);

    //this.counter = 0;

    //initialize the state in class component
    this.state = {
      counter: 0,
    };
  }

  render() {
    const onHandleIncrease = () => {
      //alert("You increase the counter!");
      //this.counter++;
      //console.log(`Counter : ${this.counter}`);

      this.setState({
        counter: this.state.counter + 1,
      });
    };

    const onHandleDecrease = () => {
      //alert("You decrease the counter!");
      //this.counter--;
      //console.log(`Counter : ${this.counter}`);

      this.setState({
        counter: this.state.counter - 1,
      });
    };

    return (
      <div className="container w-50 text-center">
        <h3>Counter Class Component</h3>
        <p>Click Increase or Decrease for counter</p>
        <h1 className="my-5">
          <span className="bg-danger-subtle p-3 rounded shadow-sm">
            {this.state.counter}
          </span>
        </h1>
        <div>
          <button className="btn btn-success me-3" onClick={onHandleIncrease}>
            Increase
          </button>
          <button
            className="btn btn-danger"
            disabled={this.state.counter === 0}
            onClick={onHandleDecrease}
          >
            Decrease
          </button>
        </div>
      </div>
    );
  }
}

export default CounterClassComponent;

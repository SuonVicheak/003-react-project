import React, { Component } from "react";

export class LoginButton extends Component {
  constructor(props) {
    super(props);
  }

  //arrow function ensures that the method is bound with the current object
  //   handleClick = () => {
  //     console.log("handleClick is called,", this);
  //   };

  handleClick() {
    console.log("handleClick is called,", this);
  }

  render() {
    return (
      <div className="container mt-4">
        <button onClick={() => this.handleClick()} className="btn btn-warning">
          Login
        </button>
      </div>
    );
  }
}

export default LoginButton;

import React, { Component } from "react";
import EventProp from "./EventProp";
import ModeComponent from "./ModeComponent";

export class AboutComponent extends Component {
  //mounting 1
  constructor(props) {
    super(props);

    this.state = {
      name: "adminName",
      darkMode: false,
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleToggleMode = this.handleToggleMode.bind(this);

    console.log("Component mounting... constructor");
  }

  handleClick() {
    //alert("You have clicked on me!");

    this.setState({ name: "dara" });
  }

  handleToggleMode() {
    this.setState({ darkMode: !this.state.darkMode });
  }

  componentDidMount() {
    console.log("Component mounting... componentDidMount");
  }

  componentDidUpdate() {
    console.log("Component updating... componentDidUpdate");
  }

  componentWillUnmount() {
    console.log("Component unmounting... componentWillUnmount");
  }

  //mounting 2
  render() {
    console.log("Component mounting... rendering");

    return (
      <div className="container">
        <EventProp
          onClick={this.handleClick}
          name={this.state.name}
          onToggleMode={this.handleToggleMode}
        >
          This is an example of how to pass event handler via component props
        </EventProp>
        <h4 className="mb-5">{this.state.name}</h4>
        <ModeComponent darkMode={this.state.darkMode} />
      </div>
    );
  }
}

export default AboutComponent;

//component lifecycle :
//1. mounting : when a component is declared and set into the initial DOM
//(constructor -> render -> componentDidMount)
//2. updating : when a component is updated by changing its state or props (props = stateless)
//(render -> componentDidUpdate)
//3. unmounting : when a component is removed from virtual DOM
//(componentWillUnmount)
//every lifecycle stag has its own method called lifecycle method

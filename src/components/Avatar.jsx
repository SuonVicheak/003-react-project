import React from "react";

export default function Avatar() {
  const avatar =
    "https://static.vecteezy.com/system/resources/thumbnails/025/934/636/small_2x/cute-pokemon-background-icon-generated-by-ai-photo.jpg";
  const description = "Gregorio Y. Zara";
  return (
    <img
      className="avatar"
      src={avatar}
      alt={description}
      style={{ width: "150px", height: "auto" }}
    />
  );
}

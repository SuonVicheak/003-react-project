import React from "react";
import { Col } from "react-bootstrap";
import ProfileCard from "./ProfileCard";

const ProfileCardWrapper = ({ user }) => {
  return (
    <Col
      xs={12}
      sm={12}
      md={6}
      lg={4}
      xl={3}
      className="d-flex align-item-center justify-content-center"
    >
      <ProfileCard user={user} />
    </Col>
  );
};

export default ProfileCardWrapper;

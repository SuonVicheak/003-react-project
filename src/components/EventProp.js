import React, { Component } from "react";

export default class EventProp extends Component {
  render() {
    return (
      <>
        <button className="btn btn-success mx-5" onClick={this.props.onClick}>
          Click Me
        </button>
        <button className="btn btn-info" onClick={this.props.onToggleMode}>
          Toogle
        </button>
        <p className="mt-4">
          {this.props.children}, {this.props.name}
        </p>
      </>
    );
  }
}

import React from "react";

const AdminGreeting = () => {
  return (
    <div className="container">
      <h1>Welcome back Admin!</h1>
    </div>
  );
};

export default AdminGreeting;

import React from "react";
import "../styles/UserCard.css";

//mixed component 

//name export
export function Profile() {
  return <img src="https://i.imgur.com/MK3eW3As.jpg" alt="test" />;
}

//default export
export default function Gallery() {
  return (
    <section>
      <h1>Amazing scientists</h1>
      <Profile />
      <Profile />
      <Profile />
    </section>
  );
}

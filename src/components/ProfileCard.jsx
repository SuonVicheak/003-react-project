import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

function ProfileCard({ user }) {
  const { name, email, phone, imageUrl } = user;
  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={imageUrl} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          Email : {email}
          <br />
          Phone : {phone}
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default ProfileCard;

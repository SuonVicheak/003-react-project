import React, { useState } from "react";

//useState will update both the state and the UI
const CounterFuncComponent = () => {
  const [count, setCount] = useState(0);

  const onHandleIncrease = () => {
    //console.log("You increase the counter!");
    setCount(count + 1);
  };

  const onHandleDecrease = () => {
    //console.log("You decrease the counter!");
    setCount(count - 1);
  };

  return (
    <div className="container w-50 text-center">
      <h3>Counter Class Component</h3>
      <p>Click Increase or Decrease for counter</p>
      <h1 className="my-5">
        <span className="bg-danger-subtle p-3 rounded shadow-sm">{count}</span>
      </h1>
      <div>
        <button className="btn btn-success me-3" onClick={onHandleIncrease}>
          Increase
        </button>
        <button
          className="btn btn-danger"
          disabled={count === 0}
          onClick={onHandleDecrease}
        >
          Decrease
        </button>
      </div>
    </div>
  );
};

export default CounterFuncComponent;

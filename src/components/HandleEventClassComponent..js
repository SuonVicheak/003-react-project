import React, { Component } from "react";
import { Form } from "react-bootstrap";

export class HandleEventClassComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isToggleOn: true,
    };

    //binding method to make callback function works
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    console.log("Email :", e.target[0].value);
    console.log("Subject :", e.target[1].value);
    console.log("You submitted your data!");

    this.setState((prevState) => ({
      isToggleOn: !prevState.isToggleOn,
    }));
  }

  render() {
    return (
      <div className="container mb-5">
        <h1>Event Handling in React JS Class Component</h1>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="suonvicheak991@gmail.com"
              required={true}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Subject</Form.Label>
            <Form.Control as="textarea" rows={4} required={true} />
          </Form.Group>
          <button className="btn btn-primary" type="submit">
            {this.state.isToggleOn ? "ON" : "OFF"}
          </button>
        </Form>
      </div>
    );
  }
}

export default HandleEventClassComponent;

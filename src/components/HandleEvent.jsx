import React from "react";
import SampleForm from "./SampleForm";

const HandleEvent = () => {
  return (
    <div className="container">
      <h1>Event Handling in React JS</h1>
      <SampleForm />
    </div>
  );
};

export default HandleEvent;

import React, { useState } from "react";
import AlertShowBox from "./Alert";
import { useNavigate } from "react-router-dom";

const ControlledComponent = () => {
  const [email, setEmail] = useState("");
  const [fullName, setFullName] = useState("");
  const [password, setPassword] = useState("");
  const [skill, setSkill] = useState("");
  const [check, setCheck] = useState(false);
  const [gender, setGender] = useState("");
  const [image, setImage] = useState(null);
  const [isAlert, setIsAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const navigate = useNavigate(); 

  const handleImageChange = (e) => {
    //console.log(e.target.files[0]);
    //generate blob image
    if (e.target.files.length === 0) {
      console.log("Please choose any image file!");
      setImage(null);
      setIsAlert(true);
      setAlertMessage("Please choose any image file!");
      return;
    }
    if (e.target.files[0].type.includes("pdf")) {
      //console.log("invalid file type");
      console.log("Supported file type: jpeg, jpg, png, webp");
      setImage(null);
      setIsAlert(true);
      setAlertMessage("Supported file type: jpeg, jpg, png, webp");
      e.target.value = "";
      return;
    }
    let imageUrl = URL.createObjectURL(e.target.files[0]);
    //console.log(imageUrl);
    setImage(imageUrl);
    setIsAlert(false);
    setAlertMessage("");
  };

  const personObj = {
    email: email,
    fullName: fullName,
    password: password,
    skill: skill,
    check: check,
    gender: gender,
    image: image,
  };

  //console.log(isAlert);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Latest person object :", personObj);

    navigate("/");
  };

  return (
    <div className="container w-75 mb-5">
      <h3 className="bg-danger-subtle text-dark text-center my-4 shadow-sm rounded p-3">
        Please fill in the information below
      </h3>
      <div className="d-flex justify-content-between align-content-center gap-2">
        <div className="w-50">
          <div className="mb-3">
            <label htmlFor="image" className="form-label">
              <h4>Choose the image</h4>
            </label>
            <div className="my-3">
              <div
                className="bg-image hover-overlay shadow-1-strong rounded w-75"
                data-mdb-ripple-init
                data-mdb-ripple-color="light"
              >
                <img
                  src={
                    image
                      ? image
                      : "https://w0.peakpx.com/wallpaper/1010/122/HD-wallpaper-toothless-and-night-fury-artwork-night-fury-artwork-deviantart-cute-how-to-train-your-dragon.jpg"
                  }
                  className="img-fluid"
                  alt="hello"
                />
                <a href="#!">
                  <div
                    className="mask"
                    style={{ backgroundColor: "hsla(0, 0%, 98%, 0.2)" }}
                  ></div>
                </a>
              </div>
            </div>
            <input
              className="form-control w-75"
              type="file"
              id="image"
              onChange={handleImageChange}
            />
          </div>
          {isAlert && <AlertShowBox message={alertMessage} />}
        </div>
        <div className="w-50">
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="email" className="form-label">
                Email address
              </label>
              <input
                type="email"
                className="form-control"
                id="email"
                placeholder="name@example.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="fullname" className="form-label">
                Full name
              </label>
              <textarea
                className="form-control"
                id="fullname"
                rows="3"
                value={fullName}
                onChange={(e) => setFullName(e.target.value)}
              ></textarea>
            </div>
            <div className="mb-3">
              <label htmlFor="password" className="form-label">
                Password
              </label>
              <input
                type="password"
                id="password"
                className="form-control"
                aria-describedby="passwordHelpBlock"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <div id="passwordHelpBlock" className="form-text">
                Your password must be 8-20 characters long, contain letters and
                numbers, and must not contain spaces, special characters, or
                emoji.
              </div>
            </div>
            <div className="mb-3">
              <select
                className="form-select"
                aria-label="Default select example"
                onChange={(e) => setSkill(e.target.value)}
              >
                <option defaultValue={true}>Select your skill</option>
                <option value="one">One</option>
                <option value="two">Two</option>
                <option value="three">Three</option>
              </select>
            </div>
            <div className="mb-3">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="getcheck"
                  checked={check}
                  onChange={() => setCheck(!check)}
                />
                <label className="form-check-label" htmlFor="getcheck">
                  Good?
                </label>
              </div>
            </div>
            <div className="mb-3">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="gender"
                  id="male"
                  value="male"
                  onChange={(e) => setGender(e.target.value)}
                />
                <label className="form-check-label" htmlFor="male">
                  Male
                </label>
              </div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="gender"
                  id="female"
                  value="female"
                  onChange={(e) => setGender(e.target.value)}
                />
                <label className="form-check-label" htmlFor="female">
                  Female
                </label>
              </div>
            </div>
            <div className="d-flex justify-content-end align-content-between">
              <button className="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ControlledComponent;

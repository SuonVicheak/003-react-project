import React from "react";
import AdminGreeting from "./AdminGreeting";
import UserGreeting from "./UserGreeting";

function HelloGreeting({ isAdmin }) {
  return isAdmin ? <AdminGreeting /> : <UserGreeting />;
}

export default HelloGreeting;

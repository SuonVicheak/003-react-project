import React from "react";

const NotFound = () => {
  return (
    <div className="container bg-danger-subtle rounded shadow-sm mt-5 w-50">
      <h3 className="p-3">No Tasks Found</h3>
    </div>
  );
};

export default NotFound;

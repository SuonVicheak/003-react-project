import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getProductById } from "../services/productService";
import PlaceholderComponent from "./PlaceholderComponent";

const ProductDetail = () => {
  const [product, setProduct] = useState({});
  const { productId } = useParams();

  //console.log(productId);

  useEffect(() => {
    getProductById(productId)
      .then((data) => {
        setProduct(data);
        return data;
      })
      .catch((error) => console.log(error));
  }, [productId]);

  return (
    <div className="container">
      <h3>Product Detail</h3>
      {Object.keys(product).length === 0 ? (
        <PlaceholderComponent />
      ) : (
        <div>
          <h3>Product ID : {productId}</h3>
          <h3>Product Name : {product.title}</h3>
          <p>Product Description : {product.description}</p>
          <h4 className="badge rounded-pill text-bg-primary">
            Product Price : {product.price}$
          </h4>
        </div>
      )}
    </div>
  );
};

export default ProductDetail;

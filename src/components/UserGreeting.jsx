import React from "react";

const UserGreeting = () => {
  return (
    <div className="container">
      <h1>Since you are a normal user, you must follow to policies!</h1>
    </div>
  );
};

export default UserGreeting;

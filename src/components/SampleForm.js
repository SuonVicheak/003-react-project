import React, { useState } from "react";
import { Form } from "react-bootstrap";

const SampleForm = () => {
  const [isToggleOn, setIsToggleOn] = useState(true);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("event :", e);
    console.log("Email :", e.target[0].value);
    console.log("Subject :", e.target[1].value);
    console.log("You submitted your data!");

    setIsToggleOn(!isToggleOn);
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="suonvicheak991@gmail.com"
          required={true}
        />
      </Form.Group>
      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
        <Form.Label>Subject</Form.Label>
        <Form.Control as="textarea" rows={4} required={true} />
      </Form.Group>
      <button className="btn btn-primary" type="submit">
        {isToggleOn ? "ON" : "OFF"}
      </button>
    </Form>
  );
};

export default SampleForm;
